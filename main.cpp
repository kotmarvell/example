#include <iostream>
#include <string>

enum VariantType{
    BOOl,
    STRING,
    DOUBLE,
    CHAR,
    INT,
};

class Variant{

    VariantType type;
    std::string my_value;

public:
    Variant(bool value){
        type = BOOl;
        if(value == true)
            my_value = "true";
        else
            my_value = "false";
    }

    Variant(int value){
        type = INT;
        my_value = std::to_string(value);
    }

    Variant(std::string value){
        type = STRING;
        my_value = value;
    }

    Variant(double value){
        type = DOUBLE;
        my_value = std::to_string(value);
    }

    Variant(char value){
        type = CHAR;
        my_value = value;
    }

    Variant(){
        type = STRING;
        my_value = "";
    }

    VariantType getType(){
        return type;
    }
};

bool ifBool(std::string value){
    if((value == "true") || (value == "false"))
        return true;
    else
        return false;
}

bool ifString(std::string value){
    if((value[0] == '\"') && (value[value.length()] == '\"') && (value.length() > 2))
        return true;
    else
        return false;
}

bool ifDouble(std::string value){

    bool f = false;
    if (ifString(value) == false){
        for(int i = 0; i <= value.length() - 1; i++){
            if(value[i] == '.')
                f = true;
        }
     }
    return f;
}

bool ifChar(std::string value){
    if((value[0] == '\"') && (value[2] == '\"') && (!ifString(value)))
        return true;
    else
        return false;
}

bool ifInt(std::string value){
    if((!ifString(value)) && (!ifBool(value)) && (!ifChar(value))
            && (!ifDouble(value)))
        return true;
    else
        return false;
}

bool convertToBool(std::string value){
    if(value == "true")
        return true;
    else
        return false;
}

int convertToInt(std::string value){
    return std::stol(value);
}

char convertToChar(std::string value){
    return value[1];
}

double convertToDouble(std::string value){
    return std::stod(value);
}


int main()
{
    std::string var = "";
    std::cin >> var;

    Variant my;

    if (ifBool(var)){
        bool varInType = convertToBool(var);
        my = varInType;
    } else {
        if (ifString(var)){
            my = var;
        } else {
            if (ifChar(var)){
                char varInType = convertToChar(var);
                my = varInType;
            } else {
                if (ifDouble(var)){
                    double varInType = convertToDouble(var);
                    my = varInType;
                } else {
                    if (ifInt(var)){
                        int varInType = convertToInt(var);
                        my = varInType;
                    }
                }
            }
        }
    }

    std::cout << my.getType() << std::endl;
    return 0;
}
